# target group
12-16 it/statisticians

# topics

## Cloud Computing & Security

### pros & cons

+ reliability (backup/sync)
+ security
+ setup
+ cost (pay-as-you-go, licenses, etc.)
+ ease of use (management, maintenanc
+ ubiquity (availability, accessibility, sharing)
+ agility (deployment/scaling/prototyping)

- reliability (out of your control)
- security (no physical access)
- ease of use
- dependency (ToS, service)
- internet connection (reliability, bandwidth, ping)
- vendor lock-in (migration)
- lack of standards
- privacy (warrants, encryption)
- control (customization)
- transparency, debugging

### Deployment
- private
- public
- hybrid
- community

### Flavours
- cloud ("someone else's computer")
- fog (node)
- edge (device)
- dew - storage: dropbox, software: {app,play}store, platform: github

### Service Models: ?aaS
everything as a service
- IaaS infrastructure - hosting
- Paas platform - os, dev env, db, webserver (Daas desktop)
- Saas software - office 365 (Gaas games)
- Faas function - "serverless"

### Security
#### Attack Vectors
- client (hardware: keyloggers, software: virus, wetware: social engineering / human errors)
- connection (wifi, lan, man-in-the-middle, ddos)
- insiders @ vendor
- server layers (hardware, virtualization, os, software, etc.)
- VM neighbors
- law enforcement

#### Counter Measures
- deterrent (... or else!)
- prevention
  - encryption
  - good passwords, good password policies
  - multi-factor authentication
- (detective)
- (corrective)

### Vendors
- AWS
- Google
- Azure
- hosting
- paperspace
INTEGRATE IN ?AAS

## AWS

# TODO
- sources
