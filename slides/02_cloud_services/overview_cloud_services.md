## Cloud Services

### Preparation
- use password manager
- make (throwaway) Google account
  - https://accounts.google.com
  - "Use a different account"
  - "Create account" -> "for myself"
- make Microsoft account
  - https://login.microsoftonline.com
  - "No account? Create one!"
  - Use Google Email
- make DataBricks account
  - https://databricks.com/signup#signup/community
- make AWS account
  - ?

### Colab
- https://colab.research.google.com

### DataBricks
- https://community.cloud.databricks.com

### Azure
- https://notebooks.azure.com/

### Colab
- https://colab.research.google.com/

### AWS
- create keys
  - https://eu-central-1.console.aws.amazon.com/ec2/
  - Network & Security
  - Key Pairs
  - name: "my_aws_key"
- cloudera template
  - https://docs.aws.amazon.com/quickstart/latest/cloudera/deployment.html
  - https://fwd.aws/AxQMk
  - Change to Frankfurt
  - Availability zones: all
  - permitted ip range: 0.0.0.0/0
  - key: see step 1.
- emr
  - chmod 600 key-file
  - add ssh rule!

- pip install --user pyspark
- pride & prejudice:
  https://www.gutenberg.org/files/1342/1342-0.txt
- cut header & footer:
  `tail -n +32 1342-0.txt | head -n -360 > pride.txt`
- strip non-letters:
  `sed 's/[^[:alnum:]^ ]\+//g' pride.txt > pride_letters.txt`
- multiply file:
  `for i in {1..10}; do cat "pride_letters.txt"; done > pride10.txt`
- multiply file:
  `for i in {1..2000}; do cat "pride_letters.txt"; done > pride2000.txt`
- find non-zero ending:
  ` | grep -v 0$`

