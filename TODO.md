# general
## accounts
- aws
- databricks

# 01
- finalize slide texts
- images and backgrounds

# 02
- slide text
- images and backgrounds

# 03
- jupyter intro
- AWS overview

# 04
- databricks, cloudera, EMR

# 05
- review slides
- refactor slides
